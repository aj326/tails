# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: tails-l10n@boum.org\n"
"POT-Creation-Date: 2020-04-23 16:49+0000\n"
"PO-Revision-Date: 2020-09-05 06:34+0000\n"
"Last-Translator: emmapeel <emma.peel@riseup.net>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Weblate 3.8\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Unlocking and using the Persistent Storage\"]]\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/persistence.caution\" raw=\"yes\" sort=\"age\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/persistence.caution.ar\" raw=\"yes\" sort=\"age\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!toc levels=1]]\n"
msgstr "[[!toc levels=1]]\n"

#. type: Title =
#, no-wrap
msgid "Unlocking the Persistent Storage"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"When starting Tails, in the\n"
"<span class=\"guilabel\">Encrypted Persistent Storage</span> section of\n"
"the [[Welcome Screen|welcome_screen]], enter your passphrase and click\n"
"<span class=\"button\">Unlock</span>.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!img welcome_screen/persistence.png link=\"no\" alt=\"\"]]\n"
msgstr ""

#. type: Title =
#, no-wrap
msgid "Using the Persistent Storage"
msgstr ""

#. type: Plain text
msgid ""
"After you unlock the Persistent Storage, the data corresponding to each "
"feature of the Persistent Storage is automatically available. For example:"
msgstr ""

#~ msgid ""
#~ "2. Enter the passphrase of the persistent volume in the\n"
#~ "<span class=\"guilabel\">Passphrase</span> text box.\n"
#~ msgstr ""
#~ "2. Geben Sie das Kennwort für den beständigen Speicherbereich in das\n"
#~ "<span class=\"guilabel\">Passphrase</span>-Textfeld ein.\n"

#~ msgid ""
#~ "3. If you select the <span class=\"guilabel\">Read-Only</span> check box, "
#~ "the\n"
#~ "content of persistent volume will be available and you will be able to "
#~ "modify\n"
#~ "it but the changes will not be saved.\n"
#~ msgstr ""
#~ "3. Wenn Sie die <span class=\"guilabel\">Schreibgeschützt</span>-"
#~ "Auswahlbox markieren,\n"
#~ "wird der Inhalt des beständigen Speicherbereichs verfügbar sein und Sie "
#~ "werden in der Lage sein\n"
#~ "ihn zu modifizieren, die Änderungen werden jedoch nicht gespeichert.\n"
